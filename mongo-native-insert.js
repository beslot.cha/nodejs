const mongo = require('mongodb')
const assert = require('assert')
const dbHost = '127.0.0.1'
const dbPort = '27017'
const {Db, Server} = mongo
const db = new Db('local', new Server(dbHost, dbPort), {safe: true})

db.open((error, dbConnection) => {
    console.log(db._state)
    insertDocuments(db, function() {
      db.close()
  })
})

const insertDocuments = function(db, callback) {
    const collection = db.collection('documents')
    collection.insertMany([
      {url : "https://i.picsum.photos/id/1039/6945/4635.jpg?hmac=tdgHDygif2JPCTFMM7KcuOAbwEU11aucKJ8eWcGD9_Q"}, {url : "https://i.picsum.photos/id/1057/6016/4016.jpg?hmac=RjPyzbGq_MxSbghhfa1iVykXTskk9IISuzavny11_lI"}, {url : "https://i.picsum.photos/id/166/1280/720.jpg?hmac=w7NFsk0bL2IjWSdLJy0Ymow0MFw6n2BCjPYhJCgEjXs"}
    ], function(err, result) {
      assert.strictEqual(err, null)
      assert.strictEqual(3, result.result.n)
      assert.strictEqual(3, result.ops.length)
      console.log("Inserted 3 documents into the collection")
      callback(result)
    })
}

