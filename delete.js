const mongo = require('mongodb')
const assert = require('assert')
const dbHost = '127.0.0.1'
const dbPort = '27017'
const { Db, Server } = mongo
const db = new Db('local', new Server(dbHost, dbPort), { safe: true })

function Delete(item) {
  var urlDelete = '';
  urlDelete = item.getAttribute('src');
  db.open((error, dbConnection) => {
    console.log(db._state)
    db.collection('documents').deleteOne({ url: urlDelete }, function (err, result) {
      urlDelete = "";
      assert.Equal(err, null)
      assert.Equal(1, result.result.n)
    });
    db.close();
  })
}