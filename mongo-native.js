const mongo = require('mongodb')
const assert = require('assert')
const dbHost = '127.0.0.1'
const dbPort = '27017'
const {Db, Server} = mongo
const db = new Db('local', new Server(dbHost, dbPort), {safe: true})

db.open((err, dbConnection) => {
    db.collection('documents').updateOne({'url': "https://i.picsum.photos/id/1039/6945/4635.jpg?hmac=tdgHDygif2JPCTFMM7KcuOAbwEU11aucKJ8eWcGD9_Q"}, {$set: {url: 'https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U'}}, function(err, result) {
        console.log("Updated")
        assert.equal(err, null)
        assert.equal(1, result.result.n)
    })
    db.close()
})

function Test(item){
    console.log( item.getAttribute('src'));
}