const express = require('express');
const http = require('http');
const path = require('path');
const mongo = require('mongodb')
const dbHost = '127.0.0.1'
const dbPort = '27017'
const {Db, Server} = mongo
const db = new Db('local', new Server(dbHost, dbPort), {safe: true})
var arr = [];
db.open((error, dbConnection) => {
  console.log(db._state);

  db.collection('documents').find().forEach(function (result) {
    arr.push(result.url);
    db.close();
  })
})

let app = express();

app.set('appName', 'Campus Academy');
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.all('*', (req, res) => {
  res.render('index', {liste: arr})
})

http.createServer(app)
  .listen(
    app.get('port'),
    () => {
      console.log(`Express.js server ecoutes sur le port ${app.get('port')}`)
    } );

console.log(`Mon serveur est http://localhost:${app.get('port')}`);